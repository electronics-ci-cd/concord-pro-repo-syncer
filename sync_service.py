# Ari Mahpour
# 07/08/2020
# Description: This script monitors a Git server directory and
#              mirrors each repository to their respective repository
#              specified in repo_lookupionary.py
#              Note: This script should be used as a Windows service

'''
Usage: 
Install the service: $ python sync_service.py install
Update the service: $ python sync_service.py update
Run in debug (service) mode: $ python sync_service.py debug --interactive
Run from the command line (not as a service): $ python sync_service.py

Warning: Be sure to have the file C:\Program Files\Python36\Lib\site-packages\win32\pywintypes36.dll 
         (please note that “36” is the version of your Python installation). 
         If you don’t have this file, take it from C:\Program Files\Python36\Lib\site-packages\pywin32_system32\pywintypes36.dll 
         and copy it into C:\Program Files\Python36\Lib\site-packages\win32

Note: The following must be set up in the config.ini file:
1. Path to Git Server Folder
2. Altium Concord Pro Project Folder GUID
3. Bitbucket Settings
4. FirebirdDB Settings

How this script works:
1. Generate lookup table between Concord Pro and Git Server
2. Run the Watchdog
3. On every change for the directory perform a mirrored push
'''

import time
import logging
import sys
import os
import subprocess
import configparser
from pathlib import Path
from SMWinservice import SMWinservice
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from logging.handlers import RotatingFileHandler
from repo_dictionary import RepoDictionary

class RepoMirrorService(SMWinservice):
    _svc_name_ = "RepoMirrorService"
    _svc_display_name_ = "Mirroring Service for Git Repositories"
    _svc_description_ = "Windows service that mirrors Git repositories from one server to another."

    # Variables used globally within class
    observer = None
    repo_lookup = None
    config = None

    # Starting the Windows service
    def start(self):
        self.isrunning = True

        # Read in configuration file
        self.config = configparser.ConfigParser()
        self.config.read(str(os.path.dirname(os.path.realpath(__file__))) + "/config.ini")

        # Create the repository lookup table
        self.repo_lookup = RepoDictionary(firebird_settings=self.config._sections['FirebirdDB'], 
                                    project_folder_guid=self.config['ConcordPro']['project_folder_guid'],
                                    bitbucket_settings=self.config._sections['Bitbucket'])

        # Set up Watchdog rotating logger and event listener
        if not self.config['Logger']['filepath']:
            logpath = str(Path.home()) + '\\sync_service.log'
        else:
            logpath = self.config['Logger']['filepath']
        logging.basicConfig(handlers=[RotatingFileHandler(logpath, maxBytes=1000000, backupCount=5)],
                            level=logging.INFO,
                            format='%(asctime)s - %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
        event_handler = LoggingEventHandler()
        event_handler.on_any_event = self.on_any_event
							
        # Establish the observer
        self.observer = Observer()
        self.observer.schedule(event_handler, self.config['ConcordPro']['git_folder_path'], recursive=True)
        self.observer.start()

        # Build dictionary and mirror repositories.
        logging.info("Service started. Building dictionary and mirroring repositories.")
        self.repo_lookup.build_repo_dict()
        for dir in os.listdir(self.config['ConcordPro']['git_folder_path']):
            self.update_git_server(self.config['ConcordPro']['git_folder_path'] + "/" + dir)

    # Stopping the Windows service
    def stop(self):
        logging.info("Stopping service.")
        self.isrunning = False

        # Stop the observer
        self.observer.stop()
        self.observer.join()        

    # Run a function on any event that occurs
    def on_any_event(self, event):
        # Need to add a small delay in between fired events.
		# NOte: This number may need to be tweaked by the user.
        time.sleep(0.25)
        
        # Run only when the master file has been updated (i.e. updating HEAD to new Git hash)
        if (event.src_path.endswith("master")):
            self.update_git_server(event.src_path)

    # Update the target Git server
    def update_git_server(self, event_src_path):
        # Get the Altium Concord Pro repository GUID
        GUID = os.path.relpath(event_src_path, self.config['ConcordPro']['git_folder_path']).split("\\")[0].strip()

        # Rebuild the dictionary before attempting mirror operation
        self.repo_lookup.build_repo_dict()

        # Move into directory in which event occurred
        # Attempt to mirror from Concord Pro to Git server destination
        if GUID in self.repo_lookup.repo_dict:
            # Repository exists in the "Managed Projects" folder in Altium Concord Pro
            cmd = "cd " + self.config['ConcordPro']['git_folder_path'] + "/" + GUID + " && " + "git push --mirror " + self.repo_lookup.repo_dict[GUID]
            logging.info("Change detected. Running command: " + cmd)
            logging.info(subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT))
        else:
            # Managed project must exist somewhere else.
            # Note: To change this you must update the dictionary class to look in other project_folder_guid locations
            logging.warning("Target repository does not exist for: " + GUID + ". Project MUST exist in Managed Projects folder in Altium Concord Pro.")

    # Main loop once the service has started
    def main(self):
        while self.isrunning:
            time.sleep(0.01)

if __name__ == '__main__':
    # Run locally (i.e. not as a service)
    # FOR DEBUGGING PURPOSES ONLY
    if len(sys.argv) < 2:
        # Overload the class skip the Windows Service portion
        class RepoMirrorServiceDebug(RepoMirrorService):
            def __init__(self):
                pass
        
        print("Running locally (i.e. not as a service)")
        cmdline_inst = RepoMirrorServiceDebug()
        cmdline_inst.start()
        try:
            cmdline_inst.main()
        except KeyboardInterrupt:
            cmdline_inst.stop()
    else:
        RepoMirrorService.parse_command_line()
