# Ari Mahpour
# 06/18/2020
# repo_dictionary.py
# Description: This class generates a lookup table between Altium Concord Pro 
#              repositories and Bitbucket repositories.
#              Note: This script MUST be run on the Altium Concord Pro server

import re
import firebirdsql
from atlassian import Bitbucket

class RepoDictionary:
    repo_dict = {}
    firebird_conn = None
    project_folder_guid = None
    bitbucket_settings = None

    def __init__(self, firebird_settings, project_folder_guid, bitbucket_settings):
        # Establish connections to the Firebird database
        self.firebird_conn = firebirdsql.connect(
            host=firebird_settings['host'],
            database=firebird_settings['database'],
            port=firebird_settings['port'],
            user=firebird_settings['user'],
            password=firebird_settings['password']
        )
        self.project_folder_guid = project_folder_guid
        self.bitbucket_settings = bitbucket_settings

        self.build_repo_dict()

    def __del__(self):
        self.firebird_conn.close()

    # Retrive list of repositories from the Concord Pro database
    def get_concord_db_list(self):
        # Retrieve data from Firebird Database
        cur = self.firebird_conn.cursor()
        cur.execute("SELECT * FROM ALU_FOLDER WHERE PARENTFOLDERGUID = '" + self.project_folder_guid + "'")
        results = cur.fetchall()

        return results        

    # Create a lookup table between Concord Pro and Bitbucket
    def build_repo_dict(self):
        # Establish a connection to the Bitbucket server
        bitbucket_conn = Bitbucket(url=self.bitbucket_settings['base_url'], username=self.bitbucket_settings['user'], password=self.bitbucket_settings['password'])

        # Retrieve the list of repositories from Bitbucket
        bitbucket_repos = bitbucket_conn.repo_list(self.bitbucket_settings['project_key'])
        # Extract out 'slug' key and rebuild into list
        bitbucket_repos = [sub['slug'] for sub in bitbucket_repos]

        # Iterate through list of repositories on Concord Pro side
        for repo in self.get_concord_db_list():
			# Use the Altium project name as the repository name
			# Note: Cannot contain illegal characters
			bitbucketRepoName = repo[1]

			# Validate that there is a matching pair on the Bitbucket side
			if not bitbucketRepoName in bitbucket_repos:
				# Create the repository using the API
				bitbucket_conn.create_repo(project_key=self.bitbucket_settings['project_key'], repository=bitbucketRepoName, forkable=True, is_private=True)
			# Note: Username and password will show up in the command line (and in this dictionary) for the remote Git URL.
			base_url = self.bitbucket_settings['base_url'].replace('https://', 'https://' + self.bitbucket_settings['user'] + ":" + self.bitbucket_settings['password'] + "@")
			self.repo_dict[repo[0]] = base_url + "scm/" + self.bitbucket_settings['project_key'].lower() + "/" + bitbucketRepoName + ".git"
